from abc import ABC, abstractmethod
from datetime import datetime

class Person(ABC):
    @abstractmethod
    def getFullName(self):
        pass

    @abstractmethod
    def addRequest(self):
        pass

    @abstractmethod
    def checkRequest(self):
        pass

    @abstractmethod
    def addUser(self):
        pass

class Employee(Person):
    def __init__(self, firstName, lastName, email, department):
        self.firstName = firstName
        self.lastName = lastName
        self.email = email
        self.department = department

    def getFullName(self):
        return self.firstName + ' ' + self.lastName

    def addRequest(self):
        return "Request has been added"

    def checkRequest(self):
        return "Placeholder"

    def addUser(self):
        return "Placeholder"

    def login(self):
        return self.email + " has logged in"

    def logout(self):
        return self.email + " has logged out"

class TeamLead(Person):
    def __init__(self, firstName, lastName, email, department):
        self.__firstName = firstName
        self.__lastName = lastName
        self.__email = email
        self.__department = department
        self.__members = []

    def getFullName(self):
        return self.__firstName + ' ' + self.__lastName

    def addRequest(self):
        return "Request has been added"

    def checkRequest(self):
        return "Placeholder"

    def addUser(self):
        return "Placeholder"

    def login(self):
        return self.__email + " has logged in"

    def logout(self):
        return self.__email + " has logged out"

    def addMember(self, employee):
        self.__members.append(employee)

    def get_members(self):
        return self.__members

class Admin(Person):
    def __init__(self, firstName, lastName, email, department):
        self.__firstName = firstName
        self.__lastName = lastName
        self.__email = email
        self.__department = department

    def getFullName(self):
        return self.__firstName + ' ' + self.__lastName

    def addRequest(self):
        return "Request has been added"

    def checkRequest(self):
        return "Placeholder"

    def addUser(self):
        return "New user added"

    def login(self):
        return self.__email + " has logged in"

    def logout(self):
        return self.__email + " has logged out"

class Request:
    def __init__(self, name, requester, dateRequested):
        self.name = name
        self.requester = requester
        self.dateRequested = datetime.strptime(dateRequested, "%d-%b-%Y")
        self.status = "open"

    def updateRequest(self):
        return "Request has been updated"

    def closeRequest(self):
        if self.status == "closed":
            return "Request " + self.name + " has been closed"
        else:
            return "Cannot close. Request " + self.name + " is " + self.status

    def cancelRequest(self):
        self.status = "cancelled"
        return "Request has been cancelled"

    def set_status(self, status):
        self.status = status

if __name__ == "__main__":
    # This code will only run if this file is executed directly, not when it's imported
    employee1 = Employee("John", "Doe", "djohn@mail.com", "Marketing")
    employee2 = Employee("Jane", "Smith", "sjane@mail.com", "Marketing")
    employee3 = Employee("Robert", "Patterson", "probert@mail.com", "Sales")
    employee4 = Employee("Brandon", "Smith", "sbrandon@mail.com", "Sales")
    admin1 = Admin("Monika", "Justin", "jmonika@mail.com", "Marketing")
    teamLead1 = TeamLead("Michael", "Specter", "smichael@mail.com", "Sales")
    req1 = Request("New hire orientation", teamLead1, "27-Jul-2021")
    req2 = Request("Laptop repair", employee1, "1-Jul-2021")
    
    assert employee1.getFullName() == "John Doe", "Full name should be John Doe"
    assert admin1.getFullName() == "Monika Justin", "Full name should be Monika Justin"
    assert teamLead1.getFullName() == "Michael Specter", "Full name should be Michael Specter"
    assert employee2.login() == "sjane@mail.com has logged in"
    assert employee2.addRequest() == "Request has been added"
    assert employee2.logout() == "sjane@mail.com has logged out"
    
    teamLead1.addMember(employee3)
    teamLead1.addMember(employee4)
    for indiv_emp in teamLead1.get_members():
        print(indiv_emp.getFullName())
    
    assert admin1.addUser() == "New user added"
    
    req2.set_status("closed")
    print(req2.closeRequest())
